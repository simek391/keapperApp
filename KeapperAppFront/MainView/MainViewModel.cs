﻿using System;
using KeapperAppFront.InnerViews.ClientDetail;
using KeapperAppFront.InnerViews.ClientEditNew;
using KeapperAppFront.InnerViews.ClientsList;
using KeapperAppFront.InnerViews.ContactDetail;
using KeapperAppFront.InnerViews.ContactEditNew;
using KeapperAppFront.InnerViews.ContactList;
using KeapperAppFront.MainMenu;
using KeapperAppFront.Utils;
using KeapperAppFront.Utils.EventsArgs;
using KeapperAppFront.ViewModels.Utils;

namespace KeapperAppFront.MainView
{
    public class MainViewModel : BindableBase
    {
        private readonly ClientsListViewModel _clientsListViewModel = new ClientsListViewModel();
        private readonly ClientDetailViewModel _clientDetailViewModel = new ClientDetailViewModel();
        private readonly ClientEditNewViewModel _clientEditNewView = new ClientEditNewViewModel();

        private readonly ContactListViewModel _contactListViewModel = new ContactListViewModel();
        private readonly ContactDetailViewModel _contactDetailViewModel = new ContactDetailViewModel();
        private readonly ContactEditNewViewModel _contactEditNewViewModel = new ContactEditNewViewModel();


        #region Commands
        public RelayCommand<string> NavCommand { get; private set; }
        #endregion

        #region Properties
        private BindableBase _currentViewModel;
        public BindableBase CurrentViewModel
        {
            get => _currentViewModel;
            set => SetProperty(ref _currentViewModel, value);
        }

        private BindableBase _mainMenuViewModel;
        public BindableBase MainMenuViewModel
        {
            get => _mainMenuViewModel;
            set => SetProperty(ref _mainMenuViewModel, value);
        }
        #endregion

        #region Constructor
        public MainViewModel()
        {
            _mainMenuViewModel = new MainMenuViewModel(this); ;
            _currentViewModel = null;
            NavCommand = new RelayCommand<string>(OnNav);

            NavEventBinding();
        }
        #endregion

        #region Navigation
        private void OnNav(string destination)
        {
            switch (destination)
            {
                case "ClientsList":
                    NavToClientList();
                    break;
                case "ContactsList":
                    NavToContactList();
                    break;
            }
        }
        
        private void NavEventBinding()
        {
            _clientsListViewModel.ClientView_ViewCommandSelected += NavToClientDetails;
            _clientsListViewModel.ClientView_NewEditCommandSelected += NavToClientNewEditNew;
            _clientEditNewView.ClientDetail_CancelOrSaveCommandSelected += NavToClientCallback;

            _contactListViewModel.ContactView_ViewCommandSelected += NavToContactDetails;
            _contactListViewModel.ContactView_NewEditCommandSelected += NavToContactNewEditNew;
            _contactEditNewViewModel.ContactDetail_CancelOrSaveCommandSelected += NavToContactCallback;


        }
        #endregion

        //TODO: FIND GOOD WAY TO SPLIT INTO MULTIPLE CLASSES
        #region Naviagiton{Client}

        private void NavToClientList()
        {
            CurrentViewModel = _clientsListViewModel;
        }

        private void NavToClientDetails(object sender, NavEventArgs navEventArgs)
        {
            _clientDetailViewModel.SelectedId = navEventArgs.SelectedId;
            CurrentViewModel = _clientDetailViewModel;
        }

        private void NavToClientNewEditNew(object sender, NavEventArgs navEventArgs)
        {
            _clientEditNewView.SelectedId = navEventArgs.SelectedId;
            CurrentViewModel = _clientEditNewView;
        }

        private void NavToClientCallback(object o, NavEventArgs navEventArgs)
        {
            BindableBase callbackViewModel = navEventArgs.CallbackViewModel;
            if (callbackViewModel == null)
            {
                CurrentViewModel = _clientsListViewModel;
                return;
            }
            CurrentViewModel = callbackViewModel;
        }
        #endregion

        #region Naviagiton{Contact}
        private void NavToContactList()
        {
            CurrentViewModel = _contactListViewModel;
        }

        private void NavToContactDetails(object sender, NavEventArgs navEventArgs)
        {
            _contactDetailViewModel.SelectedId = navEventArgs.SelectedId;
            CurrentViewModel = _contactDetailViewModel;
        }

        private void NavToContactNewEditNew(object sender, NavEventArgs navEventArgs)
        {
            _contactEditNewViewModel.SelectedId = navEventArgs.SelectedId;
            CurrentViewModel = _contactEditNewViewModel;
        }

        private void NavToContactCallback(object o, NavEventArgs navEventArgs)
        {
            BindableBase callbackViewModel = navEventArgs.CallbackViewModel;
            if (callbackViewModel == null)
            {
                CurrentViewModel = _contactListViewModel;
                return;
            }
            CurrentViewModel = callbackViewModel;
        }
        #endregion
    }
}