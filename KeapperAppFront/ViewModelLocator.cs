﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KeapperAppFront
{
    public static class ViewModelLocator
    {
        public static bool GetAutoWireViewModel<T>(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoWireViewModelProperty) ;
        }

        public static void SetAutoWireViewModel(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoWireViewModelProperty, value);
        }

        public static readonly DependencyProperty AutoWireViewModelProperty =
            DependencyProperty.RegisterAttached(
                "AutoWireViewModel",
                typeof(bool),
                typeof(ViewModelLocator),
                new PropertyMetadata(false , AutoWireViewModelChanged)
            );

        private static void AutoWireViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(DesignerProperties.GetIsInDesignMode(d)) return;
            var viewType = d.GetType();
            var viewTypeName = viewType.FullName;
            var viewModelTypeName = viewTypeName + "Model";
            var viewModelType = Type.GetType(viewModelTypeName);
            if(viewModelType == null) return;
            var viewModel = Activator.CreateInstance(viewModelType);
            if (d is FrameworkElement view) view.DataContext = viewModel;
        }
    }
}
