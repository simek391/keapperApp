﻿using System;

namespace KeapperAppFront.Utils.EventsArgs
{

    //TODO: THINK ABOUT SPLIT INTO TWO DIFFERENT ARGS
    public class NavEventArgs : EventArgs 
    {
        public BindableBase CallbackViewModel { get; private set; }
        public long? SelectedId { get; private set; }

        public NavEventArgs(){}

        public NavEventArgs(BindableBase callbackViewModel)
        {
            CallbackViewModel = callbackViewModel;
        }

        public NavEventArgs(long? selectedId)
        {
            this.SelectedId = selectedId;
        }
    }
}