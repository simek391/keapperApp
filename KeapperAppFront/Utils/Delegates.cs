﻿using KeapperAppFront.Utils.EventsArgs;

namespace KeapperAppFront.Utils
{
    public delegate void OnNavEventHandler(object sender, NavEventArgs e);
}