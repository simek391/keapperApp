﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using KeapperAppBack;
using KeapperAppBack.Config;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppFront.InnerViews.ClientEditNew;
using KeapperAppFront.InnerViews.GenericViewModel;
using KeapperAppFront.MainView;
using KeapperAppFront.Utils;
using KeapperAppFront.Utils.EventsArgs;
using KeapperAppFront.ViewModels.Utils;

namespace KeapperAppFront.InnerViews.ClientsList
{
    public class ClientsListViewModel : GenericListViewModel<Client>
    {
        #region Command
        public RelayCommand ViewCommand { get; set; }
        public RelayCommand NewCommand { get; set; }
        public RelayCommand EditCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        #endregion



        #region Constructors&Initialization
        public ClientsListViewModel()
        {
            InitActions();
        }       
        
        private void InitActions()
        {
            ViewCommand = new RelayCommand(OnView, CanView);
            NewCommand = new RelayCommand(OnNew, CanNew);
            EditCommand = new RelayCommand(OnEdit, CanEdit);
            DeleteCommand = new RelayCommand(OnDelete, CanDelete);
        }
        #endregion


        protected override void RiseCanExecuteChanged()
        {
            DeleteCommand.RaiseCanExecuteChanged();
            EditCommand.RaiseCanExecuteChanged();
            NewCommand.RaiseCanExecuteChanged();
            ViewCommand.RaiseCanExecuteChanged();
        }

        #region ViewAction
        public event OnNavEventHandler ClientView_ViewCommandSelected = delegate(object sender, NavEventArgs e) {  };

        private bool CanView() => SelectedItem != null;
        
        private void OnView()
        {
            ClientView_ViewCommandSelected(this, new NavEventArgs(SelectedItem.Id));
        }
        #endregion

        #region New&EditAction
        public event OnNavEventHandler ClientView_NewEditCommandSelected = delegate(object sender, NavEventArgs e) {  };

        private bool CanEdit() => SelectedItem != null;

        private void OnEdit()
        {
            ClientView_NewEditCommandSelected(this, new NavEventArgs(SelectedItem.Id));
        }

        private bool CanNew() => true;

        private void OnNew()
        {
            ClientView_NewEditCommandSelected(this, new NavEventArgs());;
        }
        #endregion

        #region DeleteAction
        private bool CanDelete() => SelectedItem != null;

        private void OnDelete()
        {
            Repository.Remove(SelectedItem);
        }
        #endregion
    }
}
