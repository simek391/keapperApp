﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeapperAppBack;
using KeapperAppBack.Config;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppFront.InnerViews.GenericViewModel;
using KeapperAppFront.Utils;
using KeapperAppFront.Utils.EventsArgs;

namespace KeapperAppFront.InnerViews.ClientEditNew
{
    public class ClientEditNewViewModel: GenericEditNewViewModel<Client>
    {
        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }



        #region Initialization&OnLoad
        public ClientEditNewViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            SaveCommand = new RelayCommand(OnSave,CanSave);
            CancelCommand = new RelayCommand(OnCancel,CanCancel);
        }
        #endregion


        public event OnNavEventHandler ClientDetail_CancelOrSaveCommandSelected = delegate(object sender, NavEventArgs e) {  };

        protected override void OnSave()
        {
            base.OnSave();
            ClientDetail_CancelOrSaveCommandSelected(this, new NavEventArgs(CallbackViewModel));
        }

        protected override void OnCancel()
        {
            base.OnCancel();
            ClientDetail_CancelOrSaveCommandSelected(this, new NavEventArgs(CallbackViewModel));
        }

        protected override void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SaveCommand.RaiseCanExecuteChanged();
        }
    }
}
