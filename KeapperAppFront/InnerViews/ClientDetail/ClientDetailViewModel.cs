﻿using System.Linq;
using KeapperAppBack;
using KeapperAppBack.Config;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppFront.InnerViews.GenericViewModel;
using KeapperAppFront.Utils;

namespace KeapperAppFront.InnerViews.ClientDetail
{
    public class ClientDetailViewModel : GenericDetailViewModel<Client>
    {
        public ClientDetailViewModel() 
        {
        }
    }
}