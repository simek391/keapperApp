﻿using KeapperAppBack.Model.Entity;
using KeapperAppFront.InnerViews.GenericViewModel;
using KeapperAppFront.Utils;
using KeapperAppFront.Utils.EventsArgs;

namespace KeapperAppFront.InnerViews.ContactList
{
    public class ContactListViewModel : GenericListViewModel<Contact>
    {
        #region Command
        public RelayCommand ViewCommand { get; set; }
        public RelayCommand NewCommand { get; set; }
        public RelayCommand EditCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        #endregion



        #region Constructors&Initialization
        public ContactListViewModel()
        {
            InitActions();
        }       
        
        private void InitActions()
        {
            ViewCommand = new RelayCommand(OnView, CanView);
            NewCommand = new RelayCommand(OnNew, CanNew);
            EditCommand = new RelayCommand(OnEdit, CanEdit);
            DeleteCommand = new RelayCommand(OnDelete, CanDelete);
        }
        #endregion


        protected override void RiseCanExecuteChanged()
        {
            DeleteCommand.RaiseCanExecuteChanged();
            EditCommand.RaiseCanExecuteChanged();
            NewCommand.RaiseCanExecuteChanged();
            ViewCommand.RaiseCanExecuteChanged();
        }

        #region ViewAction
        public event OnNavEventHandler ContactView_ViewCommandSelected = delegate(object sender, NavEventArgs e) {  };

        private bool CanView() => SelectedItem != null;
        
        private void OnView()
        {
            ContactView_ViewCommandSelected(this, new NavEventArgs(SelectedItem.Id));
        }
        #endregion

        #region New&EditAction
        public event OnNavEventHandler ContactView_NewEditCommandSelected = delegate(object sender, NavEventArgs e) {  };

        private bool CanEdit() => SelectedItem != null;

        private void OnEdit()
        {
            ContactView_NewEditCommandSelected(this, new NavEventArgs(SelectedItem.Id));
        }

        private bool CanNew() => true;

        private void OnNew()
        {
            ContactView_NewEditCommandSelected(this, new NavEventArgs());;
        }
        #endregion

        #region DeleteAction
        private bool CanDelete() => SelectedItem != null;

        private void OnDelete()
        {
            Repository.Remove(SelectedItem);
        }
        #endregion
    }
}