﻿using System.Runtime.InteropServices;
using KeapperAppBack;
using KeapperAppBack.Config;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppFront.Utils;

namespace KeapperAppFront.InnerViews.GenericViewModel
{
    public class GenericListViewModel<T> : BindableBase  where T: class, IEntity, new()
    {
        #region Properties
        private Repository<T> _repository;
        public Repository<T> Repository
        {
            get => _repository;
            set => SetProperty(ref _repository, value);
        }

        private T _selectedItem;
        public T SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                RiseCanExecuteChanged();
            }
        }

        protected virtual void RiseCanExecuteChanged() {}

        //TODO: Add FACTORY to initialize Repository base on app config
        public virtual async void OnLoad()
        {
            Repository = new SpringRepository<T>(new LocalSpringRepositoryConfig<T>());
            await Repository.QueryAsync();
            OnPropertyChanged("Repository");
        }
        #endregion
    }
}