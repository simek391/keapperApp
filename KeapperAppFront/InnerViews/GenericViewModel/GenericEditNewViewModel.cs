﻿using System;
using System.ComponentModel;
using KeapperAppBack;
using KeapperAppBack.Config;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppFront.Utils;

namespace KeapperAppFront.InnerViews.GenericViewModel
{
    public class GenericEditNewViewModel<T> : BindableBase  where T: class, IEntity, new()
    {
        protected Repository<T> _Repository;

        public Boolean IsEditMode
        {
            get => _selectedId != null;
        }

        protected long? _selectedId;
        public long? SelectedId
        {
            get => _selectedId;
            set => _selectedId = value;
        }

        protected T _item;
        public T Item
        {
            get => _item;
            set => SetProperty(ref _item, value);
        }

        protected BindableBase _callbackViewModel;
        public BindableBase CallbackViewModel
        {
            get => _callbackViewModel;
            set => SetProperty(ref _callbackViewModel, value);
        }

        public GenericEditNewViewModel()
        {
            _Repository = new SpringRepository<T>(new LocalSpringRepositoryConfig<T>());
            _item = new T();
        }

        public virtual async void OnLoad()
        {
  
            if (!IsEditMode)
            {
                OnLoadForNew();
                return;
            }
            await OnLoadForEdit();
        }
        
        private async System.Threading.Tasks.Task OnLoadForEdit()
        {
            await _Repository.QueryAsync();
            Item = _Repository.FindOne(p => p.Id == SelectedId) ?? new T();
            Item.SubscribeOnChangeEvent(OnItemPropertyChanged);
        }

        private void OnLoadForNew()
        {
            Item = new T();
            _Repository.SetOne(Item);
            Item.SubscribeOnChangeEvent(OnItemPropertyChanged);
        }

        protected virtual void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        
        #region Destruction
        ~GenericEditNewViewModel()
        {
            OnClose();
        }
        public void OnClose()
        {
            if (_item == null) return;
            Item.UnSubscribeOnChangeEvent(OnItemPropertyChanged);
            _item = null;
        }
        #endregion

        protected virtual bool CanSave() => _item != null && !_item.HasErrors;
        protected virtual void OnSave()
        {
            _Repository.Commit();
            OnClose();
        }



        protected virtual bool CanCancel() => true;

        protected virtual void OnCancel()
        {
            OnClose();
        }
    }
}