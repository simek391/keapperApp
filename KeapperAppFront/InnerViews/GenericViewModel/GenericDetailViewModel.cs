﻿using System.Linq;
using KeapperAppBack;
using KeapperAppBack.Config;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppFront.Utils;

namespace KeapperAppFront.InnerViews.GenericViewModel
{
    public class GenericDetailViewModel<T> : BindableBase  where T: class, IEntity, new()
    {
        protected Repository<T> Repository;

        private long? _selectedId;
        public long? SelectedId
        {
            get => _selectedId;
            set => _selectedId = value;
        }

        private T _item;
        public T Item
        {
            get => _item;
            set => SetProperty(ref _item, value);
        }

        public virtual async void OnLoad()
        {
            if (_selectedId == null) return;
            Repository = new SpringRepository<T>(new LocalSpringRepositoryConfig<T>());
            await Repository.QueryAsync();
            Item = Repository.Find(p => p.Id == SelectedId).FirstOrDefault() ?? new T();
        }
    }
}