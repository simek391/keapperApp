﻿using System.ComponentModel;
using KeapperAppBack.Model.Entity;
using KeapperAppFront.InnerViews.GenericViewModel;
using KeapperAppFront.Utils;
using KeapperAppFront.Utils.EventsArgs;

namespace KeapperAppFront.InnerViews.ContactEditNew
{
    public class ContactEditNewViewModel : GenericEditNewViewModel<Contact>
    {
        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        #region Initialization&OnLoad
        public ContactEditNewViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            SaveCommand = new RelayCommand(OnSave,CanSave);
            CancelCommand = new RelayCommand(OnCancel,CanCancel);
        }
        #endregion

        public event OnNavEventHandler ContactDetail_CancelOrSaveCommandSelected = delegate(object sender, NavEventArgs e) {  };
        
        protected override void OnSave()
        {
            base.OnSave();
            ContactDetail_CancelOrSaveCommandSelected(this, new NavEventArgs(CallbackViewModel));
        }

        protected override void OnCancel()
        {
            base.OnCancel();
            ContactDetail_CancelOrSaveCommandSelected  (this, new NavEventArgs(CallbackViewModel));
        }

        protected override void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SaveCommand.RaiseCanExecuteChanged();
        }
    }
}