﻿using KeapperAppFront.MainView;
using KeapperAppFront.Utils;

namespace KeapperAppFront.MainMenu
{
    public class MainMenuViewModel : BindableBase
    {
        public MainViewModel MainViewModel { get; private set; }

        public MainMenuViewModel(MainViewModel mainViewModel) => MainViewModel = mainViewModel;
    }
}