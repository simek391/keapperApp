﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace System.Collections.Specialized
{
    [Serializable]
    public class TypeDictionary<TValue> : Dictionary<Type, TValue>
    {
        public TypeDictionary()
        {
        }

        //TODO:Implement
        protected TypeDictionary(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        public TValue Get<T>()
        {
            return this[typeof(T)];
        }

        public void Add<T>(TValue value)
        {
            Add(typeof(T), value);
        }

        public bool Remove<T>()
        {
            return Remove(typeof(T));
        }

        public bool TryGetValue<T>(out TValue value)
        {
            return TryGetValue(typeof(T), out value);
        }

        public bool ContainsKey<T>()
        {
            return ContainsKey(typeof(T));
        }
    }
}
