﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using KeapperAppBack.Model.Entity.OutputFormatter;
using Newtonsoft.Json;

namespace KeapperAppBack.Model.Entity
{
    public abstract class Entity : IEntity
    {
        //TODO: Add MEF, after study...
        IOutputFormatter outputFormatter;

        [JsonProperty()]
        public bool IsChanged { get; set; }

        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

       

        public Entity(IOutputFormatter outputFormatter)
        {
            this.outputFormatter = outputFormatter;
        }

        public Entity() : this(new JsonOutputFormatter())
        {
                
        }

        public void SetProperty<T>(ref T property, T value, [CallerMemberName] String propertyName = "")
        {
            var oldValue = property;
            property = value; 
            NotifyPropertyChanged(oldValue, property, propertyName);
        }
        
        public string getStringValue()
        {
            return outputFormatter.generateOutput(this);
        }


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void NotifyPropertyChanged<T>(T oldValue, T newValue, [CallerMemberName] String propertyName = "")
        {
            if (oldValue == null) return;
            if (EqualityComparer<T>.Default.Equals(oldValue, newValue)) return;
            if (EqualityComparer<T>.Default.Equals(oldValue, default(T))) return;
    
            IsChanged = true;
            Validate();
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        //TODO: FIND OTHER WAY TO SUBSCRIBE ONCE
        public void SubscribeOnChangeEvent(PropertyChangedEventHandler changedEventHandler)
        {
            PropertyChanged -= changedEventHandler;
            PropertyChanged += changedEventHandler;
        }

        public void UnSubscribeOnChangeEvent(PropertyChangedEventHandler changedEventHandler)
        {
            PropertyChanged -= changedEventHandler;
        }
        #endregion

        #region Equals
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }

            Entity entity = obj as Entity;
            if((System.Object)entity == null)
            {
                return false;
            }

            return Id == entity.Id;
        }

        public bool Equals(Entity entity)
        {
            if ((object)entity == null)
            {
                return false;
            }

            return Id == entity.Id;
        }
        #endregion

        #region INotifyDataErrorInfo
        private ConcurrentDictionary<string, List<string>> _errors = new ConcurrentDictionary<string, List<string>>();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged = delegate {};

        public void OnErrorChanged(string propertyName)
        {
            ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errorsForName;
            _errors.TryGetValue(propertyName, out errorsForName);
            return errorsForName;
        }

        public bool HasErrors => _errors.Count > 0;

        public Task ValidateAsync()
        {
            return Task.Run(() => Validate());
        }

        private object _lock = new object();
        public void Validate()
        {
            lock (_lock)
            {
                var validationContext = new ValidationContext(this, null, null);
                var validationResults = new List<ValidationResult>();
                Validator.TryValidateObject(this, validationContext, validationResults, true);

                foreach (var kv in _errors.ToList())
                {
                    if (validationResults.All(r => r.MemberNames.All(m => m != kv.Key)))
                    {
                        List<string> outLi;
                        _errors.TryRemove(kv.Key, out outLi);
                        OnErrorChanged(kv.Key);
                    }
                }

                var errorsGroupedByPropertyName = validationResults
                    .SelectMany(rule => rule.MemberNames,
                        (rule, name) => new {PropertyName = name, Error = rule.ErrorMessage})
                    .GroupBy(errors => errors.PropertyName);

                foreach (var errorsGroup in errorsGroupedByPropertyName)
                {
                    List<string> errorMessages = errorsGroup.Select(error => error.Error).ToList();
                    _errors.AddOrUpdate(
                        errorsGroup.Key,
                        errorMessages,
                        (s, list) => {list.Add(s); return list;}
                    );
                    OnErrorChanged(errorsGroup.Key);
                }
            }
        }
        #endregion
    }
}
