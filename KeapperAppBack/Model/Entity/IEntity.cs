﻿using System.ComponentModel;

namespace KeapperAppBack.Model.Entity
{
    public interface IEntity : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        long? Id { get; set; }
        bool IsChanged { get; set; }
        void SubscribeOnChangeEvent(PropertyChangedEventHandler changedEventHandler);
        void UnSubscribeOnChangeEvent(PropertyChangedEventHandler changedEventHandler);
    }
}
