﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeapperAppBack.Model.Entity.OutputFormatter
{
    public interface IOutputFormatter
    {
        string generateOutput(IEntity entity);
    }
}
