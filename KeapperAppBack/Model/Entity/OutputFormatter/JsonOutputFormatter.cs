﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeapperAppBack.Model.Entity.OutputFormatter
{
    public class JsonOutputFormatter : IOutputFormatter
    {
        public string generateOutput(IEntity entity)
        {
            return JsonConvert.SerializeObject(entity);
        }
    }
}
