﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeapperAppBack.Model.Entity.OutputFormatter
{
    public class ToStringOutputFormatter : IOutputFormatter
    {
        public string generateOutput(IEntity entity)
        {
            return entity.ToString();
        }
    }
}
