﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KeapperAppBack.Model.Entity.OutputFormatter;
using Newtonsoft.Json;

namespace KeapperAppBack.Model.Entity
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Client : Entity
    {
        public Client(IOutputFormatter outputFormatter) : base(outputFormatter) { }

        public Client() : base()
        {

        }
        
        private string _name;  
        [JsonProperty(PropertyName = "name")]
        [Required()]
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private List<AreaOfInterests> _areaOfInterests;  
        [JsonProperty(PropertyName = "areaOfInterests")]
        [Required()]
        public List<AreaOfInterests> AreaOfInterests
        {
            get => _areaOfInterests;
            set => SetProperty(ref _areaOfInterests, value);
        }

        public override bool Equals(object obj)
        {
            return obj is Client client &&
                   base.Equals(obj) &&
                   _name == client._name;
        }

        public override int GetHashCode()
        {
            var hashCode = -1023775901;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_name);
            return hashCode;
        }
    }
}
