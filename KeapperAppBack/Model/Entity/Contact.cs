﻿using Microsoft.Build.Framework;
using Newtonsoft.Json;

namespace KeapperAppBack.Model.Entity
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Contact : Entity
    {
        private string _firstName;
        [JsonProperty(PropertyName = "firstName")]
        [Required()]
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        private string _lastName;
        [JsonProperty(PropertyName = "lastName")]
        [Required()]
        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        protected bool Equals(Contact other)
        {
            return base.Equals(other) && string.Equals(_firstName, other._firstName) && string.Equals(_lastName, other._lastName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Contact) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (_firstName != null ? _firstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (_lastName != null ? _lastName.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}