﻿using System.ComponentModel.DataAnnotations;
using KeapperAppBack.Model.Entity.OutputFormatter;
using Newtonsoft.Json;

namespace KeapperAppBack.Model.Entity
{
    public class AreaOfInterests: Entity
    {
        public AreaOfInterests(IOutputFormatter outputFormatter) : base(outputFormatter) { }

        public AreaOfInterests() : base()
        {

        }

        private string _name;  
        [JsonProperty(PropertyName = "name")]
        [Required()]
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        protected bool Equals(AreaOfInterests other)
        {
            return base.Equals(other) && string.Equals(_name, other._name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AreaOfInterests) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (_name != null ? _name.GetHashCode() : 0);
            }
        }
    }
}