﻿using KeapperAppBack.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using AsyncBridge;
using System.Linq;
using System.Net.Http.Headers;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories;
using KeapperAppBack.Model.Repositories.RepositoriesConfig;

namespace KeapperAppBack  
{
    public class SpringRepository<T> : Repository<T> where T: IEntity, new()
    {
        #region Variable
        private readonly ISpringRepositoryConfig<T> _config;
        private static readonly HttpClient HttpClient = new HttpClient();

        protected List<long?> IdsToDelete = new List<long?>();
        #endregion

        #region Constructors
        public SpringRepository(ISpringRepositoryConfig<T> config)
        {
            this._config = config;
        }

        public SpringRepository() : this(new LocalSpringRepositoryConfig<T>()) { }
        #endregion

        #region QueryAction
        public override async Task<List<T>> QueryAsync()
        {
            Clear();
            var response = await HttpClient.GetStringAsync(_config.BuildGetEndpointAddress());
            ListOfObjects = JsonConvert.DeserializeObject<List<T>>(response);
            FireCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            return ListOfObjects;
        }

        public override List<T> Query()
        {
            using (var a = AsyncHelper.Wait)
            {
                a.Run(QueryAsync(), res => ListOfObjects = res);
            }
            return ListOfObjects;
        }
        #endregion

        #region Create/Update/Remove Actions Utils
        private StringContent PrepareContent(string body)
        {
            StringContent content = new StringContent(body, Encoding.UTF8, "application/json");
            return content;
        }

        public StringContent BuildStringContent(T entity)
        {
            var body = JsonConvert.SerializeObject(entity);
            return PrepareContent(body);
        }
        #endregion

        #region ProcessCreate Action
        private Task ProcessCreate()
        {
            var itemsToCreate = _listOfObjects.Where(i => i.Id == null)
                                              .Select(i => i);

            var createTasks = itemsToCreate.Select(i =>
            {
                var endpoint = _config.BuildPostEndpointAddress();
                var content = BuildStringContent(i);
                return HttpClient.PostAsync(endpoint, content);
            });

            return Task.WhenAll(createTasks);
        }
        #endregion

        #region ProcessUpdates Action
        private Task ProcessUpdates()
        {
            var itemsToUpdate = _listOfObjects.Where(i => i.IsChanged == true)
                                              .Select(i => i);

            var updateTasks = itemsToUpdate.Select(i =>
            {
                var endpoint = _config.BuildPutEndpointAddress();
                var content = BuildStringContent(i);
                return HttpClient.PutAsync(
                        endpoint,
                        content
                    );
            });

            return Task.WhenAll(updateTasks);
        }
        #endregion

        #region ProcessRemove Action
        private Task ProcessRemove()
        {
            var deleteTasks = IdsToDelete.Select(id => HttpClient.DeleteAsync(_config.BuildDeleteEndpointAddress() + "/" + id));

            return Task.WhenAll(deleteTasks);
        }
        #endregion

        #region CommitAction
        //TODO: add transaction mechanism
        public override async Task<IList<string>> CommitAsync()
        {
            await ProcessRemove();
            await Task.WhenAll(ProcessCreate(), ProcessUpdates());
            
            //TODO: Add error handling
            return new List<string>();
        }

        public override IList<string> Commit()
        {
            IList<string> listOfErrors = new List<string>();
            using (var a = AsyncHelper.Wait)
            {
                a.Run(CommitAsync(), res => listOfErrors = res);
            }
            return listOfErrors;
        }
        #endregion

        #region CollectionOperation(Add, Remove, Find, Set)
        public override void Add(T item)
        {
            ListOfObjects.Add(item);
            if (item.Id != null && IdsToDelete.Contains(item.Id)){
                IdsToDelete.Remove(item.Id);
            }
            FireCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        public override void Remove(T item)
        {
            if (item == null) return;
            var itemIndex = ListOfObjects.IndexOf(item);
            ListOfObjects.Remove(item);
            IdsToDelete.Add(item.Id);
            FireCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, itemIndex));
        }

        //TODO: ADD FireCollectionChanged
        public override void Remove(Predicate<T> predicate)
        {
            var deletedItems = new List<T>();
            for(var i = ListOfObjects.Count -1; i >= 0; i--)
            {
                var entity = ListOfObjects[i];
                if (predicate(entity))
                {
                    deletedItems.Add(entity);
                    Remove(entity);
                }
            }
        }

        public override List<T> Find(Predicate<T> predicate)
        {
            return ListOfObjects.Where(i => predicate(i))
                                .Select(i => i)
                                .ToList();
        }

        public override T FindOne(Predicate<T> predicate)
        {
            return Find(predicate).Single();
        }

        public override void SetList(List<T> listOfObjects)
        {
            ListOfObjects = listOfObjects;
        }

        public override void SetOne(T entity)
        {
            ListOfObjects = new List<T>(){entity};
        }

        public override void Clear()
        {
            _listOfObjects.Clear();
            FireCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override T this[int index] { get => ListOfObjects [index]; }
        #endregion
    }
}

