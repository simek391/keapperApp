﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeapperAppBack.Model.Repositories.RepositoriesConfig
{
    public interface ISpringRepositoryConfig<T>
    {
        string BuildGetEndpointAddress();
        string BuildPutEndpointAddress();
        string BuildPostEndpointAddress();
        string BuildDeleteEndpointAddress();
    }
}
