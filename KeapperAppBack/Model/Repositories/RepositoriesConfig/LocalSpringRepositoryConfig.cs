﻿using KeapperAppBack.Model.Repositories.RepositoriesConfig;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using KeapperAppBack.Model.Entity;

namespace KeapperAppBack.Config
{
    public class LocalSpringRepositoryConfig<T> : ISpringRepositoryConfig<T>
    {

        public static readonly string serverAddress = "http://localhost:8080";   
        private static readonly TypeDictionary<string> endpointsMap = new TypeDictionary<string>()
        {
            [typeof(Client)] = "/api/clients/",
            [typeof(Contact)] = "/api/contacts/"
        };

        public string BuildDeleteEndpointAddress()
        {
            return serverAddress + endpointsMap[typeof(T)];
        }

        public string BuildGetEndpointAddress()
        {
            return serverAddress + endpointsMap[typeof(T)];
        }

        public string BuildPostEndpointAddress()
        {
            return serverAddress + endpointsMap[typeof(T)];
        }

        public string BuildPutEndpointAddress()
        {
            return serverAddress + endpointsMap[typeof(T)];
        }
    }
}
