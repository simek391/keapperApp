﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;

namespace KeapperAppBack
{
    public interface IRepository<T> : IEnumerable<T>, INotifyCollectionChanged
    {
        void Add(T item);

        IList<string> Commit();
        Task<IList<string>> CommitAsync();
        
        List<T> Find(Predicate<T> p);
        T FindOne(Predicate<T> p);

        List<T> Query();
        Task<List<T>> QueryAsync();
       
        void Remove(T item);
        void Remove(Predicate<T> p);

        void SetList(List<T> listOfObjects);
        void SetOne(T entity);

        void Clear();
       
        T this[int index] { get; }
    }
}
