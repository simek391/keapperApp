﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using KeapperAppBack.Model.Entity;
using KeapperAppBack.Model.Repositories.RepositoriesConfig;

namespace KeapperAppBack.Model.Repositories
{
    public class SfdcRepository<T> : Repository<T> where T: IEntity, new()
    {
        private static readonly HttpClient HttpClient = new HttpClient();
        public SfdcRepository()
        {
        }

        public override void Add(T item)
        {
            throw new NotImplementedException();
        }

        public override IList<string> Commit()
        {
            throw new NotImplementedException();
        }

        public override Task<IList<string>> CommitAsync()
        {
            throw new NotImplementedException();
        }

        public override List<T> Find(Predicate<T> p)
        {
            throw new NotImplementedException();
        }

        public override T FindOne(Predicate<T> p)
        {
            throw new NotImplementedException();
        }

        public override List<T> Query()
        {
            throw new NotImplementedException();
        }

        public override Task<List<T>> QueryAsync()
        {
            throw new NotImplementedException();
        }

        public override void Remove(T item)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Predicate<T> p)
        {
            throw new NotImplementedException();
        }

        public override void SetList(List<T> listOfObjects)
        {
            throw new NotImplementedException();
        }

        public override void SetOne(T entity)
        {
            throw new NotImplementedException();
        }

        public override void Clear()
        {
            throw new NotImplementedException();
        }

        public override T this[int index] { get => ListOfObjects [index]; }
    }
}
