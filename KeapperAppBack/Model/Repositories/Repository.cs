﻿using KeapperAppBack.Model.Repositories.RepositoriesConfig;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace KeapperAppBack.Model.Repositories
{
    public abstract class Repository<T> : IRepository<T>
    {
        #region Variable
        protected List<T> _listOfObjects = new List<T>();
        protected List<T> ListOfObjects
        {
            get
            {
                if (_listOfObjects == null) Query();
                return _listOfObjects;
            }
            set => _listOfObjects = value;
        }
        #endregion

        public abstract void Add(T item);

        public abstract IList<string> Commit();
        public abstract Task<IList<string>> CommitAsync();
        
        public abstract List<T> Find(Predicate<T> p);
        public abstract T FindOne(Predicate<T> p);

        public abstract List<T> Query();
        public abstract Task<List<T>> QueryAsync();
       
        public abstract void Remove(T item);
        public abstract void Remove(Predicate<T> p);

        public abstract void SetList(List<T> listOfObjects);
        public abstract void SetOne(T entity);

        public abstract void Clear();

        public abstract T this[int index] { get; }

        #region IEnumerable

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        protected IEnumerator<T> GetEnumerator() => ListOfObjects.GetEnumerator();

        #endregion

        #region INotifyCollectionChanged
        public event NotifyCollectionChangedEventHandler CollectionChanged = delegate {};
        public void FireCollectionChanged(NotifyCollectionChangedEventArgs args) => CollectionChanged(this, args);
        #endregion

    }
}
